# Testing OpenPGP with Unknown Signature Versions

The OpenPGP Design Team [is concerned](https://mailarchive.ietf.org/arch/msg/openpgp/5LdYvs-PmDfbUIJ-_l6fnVesS84/) about deploying new signature versions against the deployed base.

This repository contains test vectors extracted from two tests in the [OpenPGP Interoperability test suite](https://tests.sequoia-pgp.org/).

- [Detached Signatures with unknown packets](https://tests.sequoia-pgp.org/#Detached_signatures_with_unknown_packets)
- [Messages with unknown packets](https://tests.sequoia-pgp.org/#Messages_with_unknown_packets)

All the `sig23` files here contain a signature of version 23.
OpenPGP implementations are not expected to be able to parse or verify this signature, but the presence of the unknown signature packet should not interfere with the implementation's ability to handle the rest of the data.

In particular:

- An OpenPGP implementation needs to be able to validate v4 signatures even in the presence of a signature of unknown version.
- An OpenPGP implementation needs to be able to successfully decrypt a message even if the message contains a signature of an unknown version.

## Verify Using the Stateless OpenPGP CLI

If you have a [SOP](https://datatracker.ietf.org/doc/draft-dkg-openpgp-stateless-cli/) implementation, you can verify it with:

```
make SOP=/path/to/your/sop
```

## Verify Manually

There are two types of test vector in this repository: detached signatures and encrypted messages.

### Verification of Detached Signatures

The file `signed-msg` is signed by a set of detached signatures.

Every `*.sig` file is a set of detached signatures, exactly one of which should be verifiable by Bob's OpenPGP certificate, which is found in `bob.cert`.

The exception is `sig23.sig`, which is not expected to be verifiable by any existing implementation.

### Verification of Encrypted Messages.

Every `*.pgp` file contains an encrypted, signed message.
The message can be decrypted using Bob's secret key (`bob.key`), and contains one or more signatures.

The contents of each encrypted message is the file `encrypted-msg`.

Where a v4 signature is present in any of these messages, it is made by Bob, and should be verifiable with Bob's OpenPGP certificate (`bob.cert`).

