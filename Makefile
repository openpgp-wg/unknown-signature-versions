#!/usr/bin/make -f

SOP := sop

check:
	./test-sop $(SOP)

clean:
	rm -f *.verify *.output *.log *.minimal

.PHONY: clean check
